# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)


vehicle = Vehicle.create(license_plate:'ABC342', color:'rojo', year:'2010');
route = Route.create(name:'Ruta Norte', description:'recorrido del norte',
                     category:'urbano',vehicle_id:vehicle.id)
passenger= Passenger.create(name:'Nairo Quintana', number_id:'6284729', sex:'masculino',
cellphone_number:'3016782472')
Passengers_route.create(route_id:route.id, passenger_id:passenger.id)
passenger= Passenger.create(name:'Eusebio Unzue', number_id:'689389349', sex:'masculino',
                            cellphone_number:'3016762972')
Passengers_route.create(route_id:route.id, passenger_id:passenger.id)

route = Route.create(name:'Ruta Sur', description:'recorrido del sur',
                     category:'urbano',vehicle_id:vehicle.id)
passenger= Passenger.create(name:'Rigoberto Uran', number_id:'6284729', sex:'masculino',
                            cellphone_number:'3016782472')
Passengers_route.create(route_id:route.id, passenger_id:passenger.id)
passenger= Passenger.create(name:'Luis fernando Muriel', number_id:'689389349', sex:'masculino',
                            cellphone_number:'30168992972')
Passengers_route.create(route_id:route.id, passenger_id:passenger.id)

vehicle = Vehicle.create(license_plate:'DFG334', color:'AZUL', year:'2015');
route = Route.create(name:'Ruta al mar', description:'recorrido a santa marta',
                     category:'urbano',vehicle_id:vehicle.id)
passenger= Passenger.create(name:'Shakira', number_id:'9927472', sex:'femenino',
                            cellphone_number:'3047579299')
Passengers_route.create(route_id:route.id, passenger_id:passenger.id)
passenger= Passenger.create(name:'Carlos Vives', number_id:'77279893', sex:'masculino',
                            cellphone_number:'30167678232')
Passengers_route.create(route_id:route.id, passenger_id:passenger.id)

route = Route.create(name:'Ruta calamar', description:'recorrido oriental',
                     category:'intermunicipal',vehicle_id:vehicle.id)
passenger= Passenger.create(name:'Michael Ortega', number_id:'6198301', sex:'masculino',
                            cellphone_number:'3016679072')
Passengers_route.create(route_id:route.id, passenger_id:passenger.id)
passenger= Passenger.create(name:'Teofilo Gutierrez', number_id:'82740211', sex:'masculino',
                            cellphone_number:'3029477658')
Passengers_route.create(route_id:route.id, passenger_id:passenger.id)