class CreateVehicles < ActiveRecord::Migration[5.0]
  def change
    create_table :vehicles do |t|
      t.string :license_plate
      t.string :color
      t.string :year
      t.string :category

      t.timestamps
    end
  end
end
