class CreateRoutes < ActiveRecord::Migration[5.0]
  def change
    create_table :routes do |t|
      t.string :name
      t.string :description
      t.string :category
      t.belongs_to :vehicle, index: true, foreign_key: true

      t.timestamps
    end

  end
end
