class CreatePassengers < ActiveRecord::Migration[5.0]
  def change
    create_table :passengers do |t|
      t.string :name
      t.string :number_id
      t.string :sex
      t.string :cellphone_number

      t.timestamps
    end

    create_table :passengers_routes do |t|
      t.belongs_to :route, index: true, foreign_key: true
      t.belongs_to :passenger, index: true, foreign_key: true

      t.timestamps
    end
  end
end
