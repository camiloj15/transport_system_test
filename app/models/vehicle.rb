class Vehicle < ApplicationRecord
  has_many :routes
  validates :color, presence: true
  validates :year, presence: true

  validates :license_plate, length: { is: 6 }, presence: true, uniqueness: true
  validates_format_of :license_plate, :with => /\A(ABC|DFG)((\w+))\Z/i, :on => :create

  before_create :add_category

  def add_category()
    if self.license_plate.include? "ABC"
      self.category = "1"
    else
      if self.license_plate.include? "DFG"
           self.category = "2"
      end
    end
  end
end
