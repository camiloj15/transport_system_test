class Passenger < ApplicationRecord
  has_many :passengers_routes
  has_many :routes, :through => :passengers_routes

  validates :name, presence: true,  :on => :create
  validates :number_id, presence: true, :on => :create
  validates :sex, presence: true, :on => :create
  validates :cellphone_number, presence: true, :on => :create
end
