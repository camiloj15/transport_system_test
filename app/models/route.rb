class Route < ApplicationRecord
  has_many :passengers_routes
  has_many :passengers, :through => :passengers_routes
  belongs_to :vehicle
  validates :name, presence: true,  :on => :create
  validates :description, presence: true, :on => :create
  validates :category, presence: true, :on => :create
end
