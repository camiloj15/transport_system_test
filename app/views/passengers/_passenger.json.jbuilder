json.extract! passenger, :id, :name, :number_id, :sex, :cellphone_number, :created_at, :updated_at
json.url passenger_url(passenger, format: :json)
