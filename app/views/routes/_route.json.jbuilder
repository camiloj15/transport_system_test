json.extract! route, :id, :name, :description, :category, :created_at, :updated_at
json.url route_url(route, format: :json)
